//
//  FacebookProfile.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 05/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import Foundation
import UIKit

class FacebookProfile: NSObject, Codable {
    var name:String = ""
    var profilePictureUrl:String = ""
    
    private enum CodingKeys: String, CodingKey {
        case name
        case profilePictureUrl = "url"
        case picture = "picture"
        case data = "data"
    }
    
    override init() {
        name = ""
        profilePictureUrl = ""
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        let picture = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .picture)
        let data = try picture.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        profilePictureUrl = try data.decode(String.self, forKey: .profilePictureUrl)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        var picture = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .picture)
        var data = picture.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        try data.encode(profilePictureUrl, forKey: .profilePictureUrl)
    }
}
