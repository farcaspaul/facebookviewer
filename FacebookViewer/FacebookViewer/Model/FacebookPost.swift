//
//  Post.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 07/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import Foundation
import UIKit

class FacebookPost: NSObject, Decodable {
    var creationDate:String = ""
    var id:String = ""
    var message:String? = ""
    var imageUrl:String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case creationDate = "created_time"
        case message
    }
    
    override init() {
        creationDate = ""
        id = ""
        message = ""
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
        let tempCreationDate = try container.decode(String.self, forKey: .creationDate)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss+SSSS"
        let date = dateFormatter.date(from: tempCreationDate)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        creationDate = dateFormatter.string(from: date!)
    }
}

class PostsResponse: NSObject, Decodable {
    var data:[FacebookPost] = []
    var pagingNext: String = ""
    
    private enum CodingKeys: String, CodingKey {
        case data
        case pagingNext = "next"
        case paging = "paging"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        data = try container.decode([FacebookPost].self, forKey: .data)
        let pagingContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .paging)
        pagingNext = try pagingContainer.decode(String.self, forKey: .pagingNext)
    }
}

