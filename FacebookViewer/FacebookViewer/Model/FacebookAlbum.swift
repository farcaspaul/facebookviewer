//
//  FacebookAlbum.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 05/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import Foundation
import UIKit

class FacebookAlbum: NSObject, Decodable {
    var creationDate:String = ""
    var id:String = ""
    var name:String?
    var photoUrl:String?
    var count:Int?
    var photos:[FacebookPicture] = []
    
    private enum CodingKeys: String, CodingKey {
        case id
        case creationDate = "created_time"
        case name
    }
    
    override init() {
        creationDate = ""
        id = ""
        name = ""
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        let tempCreationDate = try container.decode(String.self, forKey: .creationDate)
        let dateFormatter1: DateFormatter = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm"
        let yourDate: Date = dateFormatter1.date(from: tempCreationDate) ?? Date()
        creationDate = dateFormatter1.string(from: yourDate)
    }
}

class FacebookAlbumsResult: NSObject, Decodable {
    var albums:FacebookAlbumsDataResult?
    
    class FacebookAlbumsDataResult: NSObject, Decodable {
        var data:[FacebookAlbum]?
    }
}
