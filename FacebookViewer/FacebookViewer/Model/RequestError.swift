//
//  RequestError.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 16/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import Foundation

class RequestError: Error, Decodable {
    var message: String = ""
    
    private enum CodingKeys: String, CodingKey {
        case message
        case error = "error"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let error = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .error)
        message = try error.decodeIfPresent(String.self, forKey: .message) ?? ""
    }
}
