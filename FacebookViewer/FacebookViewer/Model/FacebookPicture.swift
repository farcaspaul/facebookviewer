//
//  FacebookPicture.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 08/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import Foundation
import UIKit

class FacebookPicture: NSObject, Decodable {
    var id:String = ""
    var pictureDescription:String?
    var pictureUrl:String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case pictureDescription = "name"
    }
    
    override init() {
        id = ""
        pictureDescription = ""
        pictureUrl = ""
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        pictureDescription = try container.decodeIfPresent(String.self, forKey: .pictureDescription) ?? ""
    }
}

class PicturesDataResult: NSObject, Decodable {
    var data:[FacebookPicture] = []
    
    private enum CodingKeys: String, CodingKey {
        case data = "data"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        data = try container.decode([FacebookPicture].self, forKey: .data)
    }
}

class PicturesResultListBySize: NSObject, Decodable {
    var images:[FacebookPhoto] = []
    
    class FacebookPhoto: NSObject, Decodable {
        var height: Int = 0
        var width: Int = 0
        var source: String = ""
    }
}
