//
//  AlbumsTableViewCell.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 12/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit

class AlbumsTableViewCell: UITableViewCell {
    
    @IBOutlet var albumLabel: UILabel!
}
