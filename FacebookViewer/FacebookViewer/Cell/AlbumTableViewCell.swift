//
//  AlbumTableViewCell.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 12/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {
    
    @IBOutlet var albumTitle: UILabel!
    @IBOutlet var albumPicturesNumber: UILabel!
    @IBOutlet var albumImage: UIImageView!
}
