//
//  PhotoCollectionViewCell.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 08/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var photo: UIImageView!
}
