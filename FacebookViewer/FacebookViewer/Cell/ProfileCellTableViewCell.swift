//
//  ProfileCellTableViewCell.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 13/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit

class ProfileCellTableViewCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var profileImage: UIImageView!
}
