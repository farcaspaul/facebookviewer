//
//  FullScreenCollectionViewCell.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 09/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit

class FullScreenCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var photo: UIImageView!
    @IBOutlet var name: UITextView!
}
