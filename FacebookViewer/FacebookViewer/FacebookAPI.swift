//
//  FacebookDataDownloader.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 06/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import Foundation
import FacebookLogin
import FBSDKLoginKit
import Alamofire
import Tapptitude

class FacebookAPI {
    
    //MARK: - profile control
    public static func downloadFacebookProfile(callback:@escaping (_ result: Tapptitude.Result<FacebookProfile>) -> Void) -> TTCancellable {
        let profile:FacebookProfile = FacebookProfile.init()
        let token = FBSDKAccessToken.current()?.tokenString ?? ""

        return Alamofire.request("https://graph.facebook.com/v3.2/me?fields=name,picture.height(300).width(300)&access_token=\(token)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
            guard let data = response.data else {
                let error = NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "Missing data"])
                callback(Tapptitude.Result.failure(error))
                return
            }
            if let value = response.value {
                if let error = try? JSONDecoder().decode(RequestError.self, from: value) {
                    callback(Tapptitude.Result.failure(error))
                    return
                }
            }
            guard let profileObject = try? JSONDecoder().decode(FacebookProfile.self, from: data) else {
                let error = NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "Missing data"])
                callback(Tapptitude.Result.failure(error))
                return
            }
            profile.name = profileObject.name
            profile.profilePictureUrl = profileObject.profilePictureUrl
//            let error = NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "DADA data"])
//            callback(Tapptitude.Result.failure(error))
            callback(Tapptitude.Result.success(profile))
        }
    }
    
    //MARK: - album control
    static public func downloadFacebookAlbums(callback:@escaping (_ albums: [FacebookAlbum]?, _ error: Error?) -> Void) {
        
        if FBSDKAccessToken.current() != nil {
            
            let token = FBSDKAccessToken.current()?.tokenString ?? ""
            
            Alamofire.request("https://graph.facebook.com/v3.2/me?fields=albums&access_token=\(token)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
                guard let data = response.data else {
                    callback(nil, response.error ?? NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "Missing data"]))
                    return
                }
                let value = response.value!
                if let error = try? JSONDecoder().decode(RequestError.self, from: value) {
                    callback(nil, error)
                    return
                }
                let facebookAlbumsResult = try! JSONDecoder().decode(FacebookAlbumsResult.self, from: data)
                let facebookAlbums = facebookAlbumsResult.albums?.data ?? []
                
                callback(facebookAlbums, nil)
            }
        }
    }
    
    static func downloadAlbumCount(album: FacebookAlbum, callback:@escaping (_: String?, _ error: Error?) -> Void) {
        
        if FBSDKAccessToken.current() != nil {
            
            let token = FBSDKAccessToken.current()?.tokenString ?? ""
            
            Alamofire.request("https://graph.facebook.com/v3.2/\(album.id)?fields=count&access_token=\(token)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                guard let data = response.data else {
                    callback(nil, response.error ?? NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "Missing data"]))
                    return
                }
                if let error = try? JSONDecoder().decode(RequestError.self, from: data) {
                    callback(nil, error)
                    return
                }
                if let value = response.value as? NSDictionary {
                    let count = value["count"] as? Int ?? 0
                    album.count = count
                    callback("\(count)", nil)
                }
            }
        }
    }
    
    static func downloadAlbumPhoto(album: FacebookAlbum, callback:@escaping (_: String?, _ error: Error?) -> Void) {
        
        if FBSDKAccessToken.current() != nil {
            let token = FBSDKAccessToken.current()?.tokenString ?? ""
            
            let params = ["access_token":token, "redirect":"false"]
            Alamofire.request("https://graph.facebook.com/v3.2/\(album.id)/picture", method: .get, parameters: params, headers: nil).responseJSON { response in
                guard let data = response.data else {
                    callback(nil, response.error ?? NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "Missing data"]))
                    return
                }
                if let error = try? JSONDecoder().decode(RequestError.self, from: data) {
                    callback(nil, error)
                    return
                }
                if let value = response.value as? NSDictionary {
                    let photoUrl = (value["data"] as? NSDictionary ?? [:])["url"] as? String ?? ""
                    album.photoUrl = photoUrl
                    callback(photoUrl, nil)
                }
            }
        }
    }
    
    static public func downloadFacebookAlbumsCount(callback:@escaping (_ count: Tapptitude.Result<Int>) -> Void) -> TTCancellable {
        
        let token = FBSDKAccessToken.current()?.tokenString ?? ""
        
        return Alamofire.request("https://graph.facebook.com/v3.2/me?fields=albums&access_token=\(token)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            guard let data = response.data else {
                let error = NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "Missing data"])
                callback(Tapptitude.Result.failure(error))
                return
            }
            if let error = try? JSONDecoder().decode(RequestError.self, from: data) {
                callback(Tapptitude.Result.failure(error))
                return
            }
            if let value = response.value as? NSDictionary {
                let albumsData = value["albums"] as? NSDictionary ?? [:]
                let albumsList = albumsData["data"] as? [NSDictionary] ?? []
                callback(Tapptitude.Result.success(albumsList.count))
//                let error = NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "DADA data"])
//                callback(Tapptitude.Result.failure(error))
            }
        }
    }
    
    static func downloadFacebookPhotoWith(id: String, bestQuality: Bool, callback:@escaping (_: String?, _ error: Error?) -> Void) {
        if FBSDKAccessToken.current() != nil {
            let token = FBSDKAccessToken.current()?.tokenString ?? ""
            
            Alamofire.request("https://graph.facebook.com/v3.2/\(id)?fields=images&access_token=\(token)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
                guard let data = response.data else {
                    callback(nil, response.error ?? NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "Missing data"]))
                    return
                }
                let value = response.value!
                if let error = try? JSONDecoder().decode(RequestError.self, from: value) {
                    callback(nil, error)
                    return
                }
                let picturesResultList = try! JSONDecoder().decode(PicturesResultListBySize.self, from: data)
                if bestQuality == true {
                    let bestPicture = picturesResultList.images.first
                    if let source = bestPicture?.source {
                        callback(source, nil)
                    }
                } else {
                    let smallestPicture = picturesResultList.images.last
                    if let source = smallestPicture?.source {
                        callback(source, nil)
                    }
                }
            }
        }
    }
    
    static func downloadFacebookPhotosFrom(album id: String, callback:@escaping (_ pictures: [FacebookPicture]?, _ error: Error?) -> Void) {
        if FBSDKAccessToken.current() != nil {
            
            let token = FBSDKAccessToken.current()?.tokenString ?? ""
            
            Alamofire.request("https://graph.facebook.com/v3.2/\(id)/photos?fields=name&access_token=\(token)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
                guard let data = response.data else {
                    callback(nil, response.error ?? NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "Missing data"]))
                    return
                }
                let value = response.value!
                if let error = try? JSONDecoder().decode(RequestError.self, from: value) {
                    callback(nil, error)
                    return
                }
                let picturesResponse = try! JSONDecoder().decode(PicturesDataResult.self, from: data)
                let facebookPictures = picturesResponse.data
                callback(facebookPictures, nil)
            }
        }
    }
    
    static func downloadFacebookPost(post: FacebookPost, callback:@escaping (_ url: String?, _ error: Error?) -> Void) {
        
        let token = FBSDKAccessToken.current()?.tokenString ?? ""
        
        Alamofire.request("https://graph.facebook.com/v3.2/\(post.id)?fields=picture&access_token=\(token)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            guard let data = response.data else {
                callback(nil, response.error ?? NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "Missing data"]))
                return
            }
            if let error = try? JSONDecoder().decode(RequestError.self, from: data) {
                callback(nil, error)
                return
            }
            
            let value = response.value as? NSDictionary ?? [:]
            let stringURL = value["picture"] as? String ?? ""
            post.imageUrl = stringURL
            callback(stringURL, nil)
        }
    }
    
    static func downloadFacebookFeed(offset: String?, callback:@escaping (_ result: Tapptitude.Result<([FacebookPost], String?)>) -> Void) -> TTCancellable? {
        let token = FBSDKAccessToken.current()?.tokenString ?? ""
        let url = offset ?? "https://graph.facebook.com/v3.2/me/feed?limit=2&access_token=\(token)"
        return Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
            guard let data = response.data else {
                let error = response.error ?? NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "Missing data"])
                callback(Tapptitude.Result.failure(error))
                return
            }
            
            if let value = response.value {
                if let error = try? JSONDecoder().decode(RequestError.self, from: value) {
                    callback(Tapptitude.Result.failure(error))
                    return
                }
            }
            
            let postsResponse = try? JSONDecoder().decode(PostsResponse.self, from: data)
            let facebookPosts = postsResponse?.data ?? []
            let next:String? = postsResponse?.pagingNext
//            let error = NSError(domain: "API", code: 212, userInfo: [NSLocalizedDescriptionKey: "DADA data"])
//            callback(Tapptitude.Result.failure(error))
            callback(Tapptitude.Result.success((facebookPosts, next)))
        }
    }
}


extension Alamofire.DataRequest: TTCancellable {
    
}
