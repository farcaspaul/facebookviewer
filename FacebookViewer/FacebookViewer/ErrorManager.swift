//
//  ErrorManager.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 07/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import Foundation
import UIKit

class ErrorManager {
    
    static func messageOf(error: Error) -> (String){
        return ((error as NSError?)?.userInfo["com.facebook.sdk:FBSDKErrorDeveloperMessageKey"] as? String) ?? error.localizedDescription
    }
    
    static func alertFor(string: String) -> (UIAlertController) {
        let alert = UIAlertController(title: "Error", message: string, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: nil))
        return alert
    }
    
    static func alertFor(error: Error) -> (UIAlertController) {
        let alert = UIAlertController(title: "Error", message: messageOf(error: error), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: nil))
        return alert
    }
}
