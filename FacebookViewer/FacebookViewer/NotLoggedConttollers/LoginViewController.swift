//
//  ViewController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 02/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit

class LoginViewController: UIViewController {
    
    @IBOutlet var loginButton: UIButton!
    
    var facebookDataDownloader = FacebookAPI()
    var currentAccessToken:FBSDKAccessToken = FBSDKAccessToken.init(tokenString: nil, permissions: nil, declinedPermissions: nil, appID: nil, userID: nil, expirationDate: nil, refreshDate: nil)
    var loginManager:LoginManager = LoginManager()
    var isMessagePresenting = { (viewController: LoginViewController) -> (Bool) in
        if let visible = viewController.presentedViewController {
            if visible.isKind(of: UIAlertController.self) {
                return true
            }
        }
        return false
    }
    
    func didReceiveErrorMessage(alert: UIAlertController) {
        if (self.navigationController?.topViewController?.isKind(of: LoginViewController.self)) ?? false {
            if isMessagePresenting(self) == false {
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loginManager = LoginManager.init()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if FBSDKAccessToken.current() != nil {
            self.loginButton.isEnabled = false
            let controller = UINavigationController.instantiateFromLoggedStoryboard()
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.changeRootController(with: controller)
        } else {
            self.loginButton.isEnabled = true
        }
    }

    //MARK: - login/logout
    @IBAction func didClickLoginButton(_ sender: Any) {
        self.loginButton.isEnabled = false
        loginManager.logIn(readPermissions: [.publicProfile, .userPhotos, .userPosts], viewController: self, completion: {(x: LoginResult) -> Void in
            if case .cancelled  = x {
                self.loginButton.isEnabled = true
            }
        })
    }
}

