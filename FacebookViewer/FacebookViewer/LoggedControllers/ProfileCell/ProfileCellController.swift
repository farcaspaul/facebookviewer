//
//  ProfileCellController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 21/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit
import Tapptitude

class ProfileCellController: CollectionCellController<FacebookProfile, ProfileCell> {
   
    var onPictureSelection: (() -> ())?

    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 180.0))
        minimumInteritemSpacing = 10
        minimumLineSpacing = 20
        sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
    
    //    custom cell size
    //    override func cellSize(for content: FacebookPost, in collectionView: UICollectionView) -> CGSize {
    //        return <#code#>
    //    }
    
    override func configureCell(_ cell: ProfileCell, for content: FacebookProfile, at indexPath: IndexPath) {
        cell.profile = content
        cell.titleLabel.text = "\(content.name)"
        cell.titleLabel.textColor = UIColor.black
        cell.imageView.kf.setImage(with: URL(string: content.profilePictureUrl))
        cell.imageView.layer.cornerRadius = 80
        cell.imageView.layer.masksToBounds = true
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        onPictureSelection?()
    }
    
    override func didSelectContent(_ content: FacebookProfile, at indexPath: IndexPath, in collectionView: UICollectionView) {
//        onPictureSelection?()
    }
}
