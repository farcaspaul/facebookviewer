//
//  ProfileViewController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 21/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit
import Tapptitude

class ProfileViewController : CollectionFeedController {
    
    init() {
        super.init(nibName: "ProfileViewController", bundle: nil)
        
        self.cellController = ProfileCellController()
//        self.dataSource = DataSource(["Test content"])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
