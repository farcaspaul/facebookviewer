//
//  ProfileCell.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 21/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit
import Tapptitude

class ProfileCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    var profile: FacebookProfile!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func handleTap() {
        let controller = PhotosInspectorViewController.instantiateFromLoggedStoryboard()
        controller.currentItem = 0
        let tempPicture = FacebookPicture()
        tempPicture.pictureUrl = profile.profilePictureUrl
        controller.pictures = [tempPicture]
        self.parentViewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
