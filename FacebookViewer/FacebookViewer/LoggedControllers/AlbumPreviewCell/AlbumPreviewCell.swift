//
//  AlbumPreviewCell.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 26/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit

class AlbumPreviewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
