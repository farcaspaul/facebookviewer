//
//  AlbumPreviewCellController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 26/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit
import Tapptitude

class AlbumPreviewCellController: CollectionCellController<FacebookAlbum, AlbumPreviewCell> {
    
    var onSelection: ((FacebookAlbum) -> ())?

    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 50.0))
    }
    
//    custom cell size
//    override func cellSize(for content: FacebookAlbum, in collectionView: UICollectionView) -> CGSize {
//        return <#code#>
//    }
    
    override func configureCell(_ cell: AlbumPreviewCell, for content: FacebookAlbum, at indexPath: IndexPath) {
        cell.titleLabel.text = "\(content.name ?? "")"
        cell.titleLabel.textColor = UIColor.black
        if let imageURL = content.photoUrl {
            cell.imageView.kf.setImage(with: URL(string: imageURL))
        } else {
            FacebookAPI.downloadAlbumPhoto(album: content) { (url, error) in
                if let imageURL = url {
                    cell.imageView.kf.setImage(with: URL(string: imageURL))
                }
            }
        }
        cell.imageView.layer.cornerRadius = 10
        cell.imageView.layer.masksToBounds = true
        if let albumCount = content.count {
            cell.subtitleLabel.text = "\(albumCount)"
        } else {
            FacebookAPI.downloadAlbumCount(album: content) { (count, error) in
                if let albumCount = count {
                    cell.subtitleLabel.text = albumCount
                }
            }
        }
    }
    
    override func didSelectContent(_ content: FacebookAlbum, at indexPath: IndexPath, in collectionView: UICollectionView) {
        onSelection?(content)
    }
}
