//
//  FacebookProfileViewController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 05/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import Foundation
import UIKit
import Tapptitude

class FacebookAlbumsViewController: UIViewController {
    
    var activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    let refreshControl = UIRefreshControl()
    var feedController = CollectionFeedController()
    @IBOutlet var collectionView: UICollectionView!
    
    var dataSource: DataSource<FacebookAlbum>? {
        return self.feedController.dataSource as? DataSource<FacebookAlbum>
    }
    
    var isMessagePresenting = { (viewController: FacebookAlbumsViewController) -> (Bool) in
        if let visible = viewController.presentedViewController {
            if visible.isKind(of: UIAlertController.self) {
                return true
            }
        }
        return false
    }
    
    func didReceiveErrorMessage(alert: UIAlertController) {
        if (self.navigationController?.topViewController?.isKind(of: FacebookAlbumsViewController.self)) ?? false {
            if isMessagePresenting(self) == false {
                self.activityView.stopAnimating()
                self.activityView.removeFromSuperview()
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    func showError(error: Error?) {
        guard let error = error else { return }
        
        if isMessagePresenting(self) == false {
            self.activityView.stopAnimating()
            self.activityView.removeFromSuperview()
            let alertController = ErrorManager.alertFor(error: error)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityView.center = self.view.center
        self.activityView.startAnimating()
        self.view.addSubview(self.activityView)
        self.view.bringSubviewToFront(self.activityView)
        FacebookAPI.downloadFacebookAlbums(callback:{( albums, error) in
            if let error = error {
                self.showError(error: error)
            } else {
                self.didDownloadAlbums(albums: albums ?? [])
            }
            self.refreshControl.endRefreshing()
        })
        feedController.creator = self

        feedController.collectionView = self.collectionView

        let albumPreviewCellController = AlbumPreviewCellController()
        albumPreviewCellController.onSelection = {(album) in
                    let controller = FacebookPicturesViewController.instantiateFromLoggedStoryboard()
                    controller.album = album
                    self.navigationController?.pushViewController(controller, animated: true)
        }

        feedController.cellController = albumPreviewCellController
        feedController.dataSource = DataSource<FacebookAlbum>()
        feedController.collectionView.refreshControl = self.refreshControl
        
        refreshControl.attributedTitle = NSAttributedString(string: "Reload albums")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    
    @objc func refresh(_ sender: Any) {
        FacebookAPI.downloadFacebookAlbums(callback:{(_ albums, error) in
            if let error = error {
                self.showError(error: error)
            } else {
                self.didDownloadAlbums(albums: albums ?? [])
            }
        })
        refreshControl.endRefreshing()
    }
    
    func didDownloadAlbums(albums: [FacebookAlbum]) {
        self.populate(with: albums)
        self.activityView.stopAnimating()
        self.activityView.removeFromSuperview()
    }
    
    //MARK: - albums data control
    public func populate(with albums: [FacebookAlbum]) -> () {
        DispatchQueue.main.async {
            self.feedController.perfomBatchUpdates({
                self.dataSource?.remove({ _ in return true })
                
                self.dataSource?.append(contentsOf: albums)
            }, animationCompletion: {})
        }
    }
}

