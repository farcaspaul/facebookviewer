//
//  PhotosInspectorViewController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 09/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import Foundation
import UIKit
import Tapptitude

class PhotosInspectorViewController: UIViewController {
    
    var pictures:[FacebookPicture] = []
    var currentItem:Int = 0
    @IBOutlet var collectionView: UICollectionView!
    let refreshControl = UIRefreshControl()
    var feedController = CollectionFeedController()
    
    var dataSource: DataSource<FacebookPicture>? {
        return self.feedController.dataSource as? DataSource<FacebookPicture>
    }
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        feedController.creator = self

        feedController.collectionView = self.collectionView
        
        let photoInspectorCellController = PhotoInspectorCellController()
        
        feedController.cellController = photoInspectorCellController
        feedController.dataSource = DataSource(pictures)
    }

    override func viewDidLayoutSubviews() {
        let indexPath = IndexPath(item: currentItem, section: 0)
        
        collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: false)
    }
}
