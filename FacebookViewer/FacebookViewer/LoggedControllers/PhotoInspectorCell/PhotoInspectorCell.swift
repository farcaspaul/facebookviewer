//
//  PhotoInspectorCell.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 27/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit

class PhotoInspectorCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
