//
//  PhotoInspectorCellController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 27/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit
import Tapptitude

class PhotoInspectorCellController: CollectionCellController<FacebookPicture, PhotoInspectorCell> {
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 100.0))
        minimumInteritemSpacing = 0
        minimumLineSpacing = 0
        sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    //    custom cell size
    override func cellSize(for content: FacebookPicture, in collectionView: UICollectionView) -> CGSize {
        let itemSize = Int(UIScreen.main.bounds.width)
        return CGSize(width: itemSize, height: itemSize)
    }
    
    override func configureCell(_ cell: PhotoInspectorCell, for content: FacebookPicture, at indexPath: IndexPath) {
        if let title = content.pictureDescription {
            cell.titleLabel.text = title
        }
        if let imageURL = content.pictureUrl {
            cell.imageView.kf.setImage(with: URL(string: imageURL))
        } else {
            FacebookAPI.downloadFacebookPhotoWith(id: content.id, bestQuality: true) { (url, error) in
                if let imageURL = url {
                    cell.imageView.kf.setImage(with: URL(string: imageURL))
                }
            }
        }
    }
    
    override func didSelectContent(_ content: FacebookPicture, at indexPath: IndexPath, in collectionView: UICollectionView) {
        
    }
}
