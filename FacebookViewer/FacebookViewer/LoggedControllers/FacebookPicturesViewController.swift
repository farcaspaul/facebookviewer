//
//  FacebookPicturesViewController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 08/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import Foundation
import UIKit
import Tapptitude

class FacebookPicturesViewController: UIViewController {
    
    var activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    var album = FacebookAlbum()
    var pictures:[FacebookPicture] = []
    let refreshControl = UIRefreshControl()
    var feedController = CollectionFeedController()
    @IBOutlet var collectionView: UICollectionView!
    
    var dataSource: DataSource<FacebookPicture>? {
        return self.feedController.dataSource as? DataSource<FacebookPicture>
    }
    var isMessagePresenting = { (viewController: FacebookPicturesViewController) -> (Bool) in
        if let visible = viewController.presentedViewController {
            if visible.isKind(of: UIAlertController.self) {
                return true
            }
        }
        return false
    }
    
    func didReceiveErrorMessage(alert: UIAlertController) {
        if (self.navigationController?.topViewController?.isKind(of: FacebookPicturesViewController.self)) ?? false {
            if isMessagePresenting(self) == false {
                self.activityView.stopAnimating()
                self.activityView.removeFromSuperview()
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func showError(error: Error?) {
        guard let error = error else { return }
        
        if isMessagePresenting(self) == false {
            self.activityView.stopAnimating()
            self.activityView.removeFromSuperview()
            let alertController = ErrorManager.alertFor(error: error)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        activityView.center = self.view.center
        FacebookAPI.downloadFacebookPhotosFrom(album: album.id, callback:{(pictures, error) in
            if let error = error {
                self.showError(error: error)
            } else {
                self.didDownloadPhotosFrom(pictures: pictures ?? [])
            }
            self.refreshControl.endRefreshing()
        })
        self.activityView.startAnimating()
        self.view.addSubview(self.activityView)
        self.view.bringSubviewToFront(self.activityView)
        
        let photoCollectionCellController = PhotoCollectionCellController()
        photoCollectionCellController.onSelection = {offset in
            let controller = PhotosInspectorViewController.instantiateFromLoggedStoryboard()
            controller.pictures = self.pictures
            controller.currentItem = offset
            self.navigationController?.pushViewController(controller, animated: true)
        }
        feedController.creator = self

        feedController.collectionView = self.collectionView
        feedController.cellController = photoCollectionCellController
        feedController.dataSource = DataSource<FacebookPicture>()
        feedController.collectionView.refreshControl = self.refreshControl
        
        refreshControl.attributedTitle = NSAttributedString(string: "Reload profile")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.collectionView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: Any) {
        FacebookAPI.downloadFacebookPhotosFrom(album: album.id, callback:{(pictures, error) in
            if let error = error {
                self.showError(error: error)
            } else {
                self.didDownloadPhotosFrom(pictures: pictures ?? [])
            }
            self.refreshControl.endRefreshing()
        })
    }
    
    func didDownloadPhotosFrom(pictures: [FacebookPicture]) {
        self.refreshControl.endRefreshing()
        self.populate(with: pictures)
        self.activityView.stopAnimating()
        self.activityView.removeFromSuperview()
    }
    
    //MARK: - pictures control
    func populate(with pictures: [FacebookPicture]) {
        self.pictures = pictures
        DispatchQueue.main.async {
            self.feedController.perfomBatchUpdates({
                self.dataSource?.remove({ _ in return true })
                
                self.dataSource?.append(contentsOf: pictures)
            }, animationCompletion: {})
        }
    }
}
