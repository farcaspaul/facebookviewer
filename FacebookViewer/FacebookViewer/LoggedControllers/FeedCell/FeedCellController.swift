//
//  FeedCellController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 21/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit
import Tapptitude

class FeedCellController: CollectionCellController<FacebookPost, FeedCell> {
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 100.0))
        minimumInteritemSpacing = 10
        minimumLineSpacing = 20
        sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
    
//    custom cell size
//    override func cellSize(for content: FacebookPost, in collectionView: UICollectionView) -> CGSize {
//        return <#code#>
//    }
    
    override func configureCell(_ cell: FeedCell, for content: FacebookPost, at indexPath: IndexPath) {
        let message = content.message ?? ""
        cell.titleLabel.text = "\(message)"
        cell.subtitleLabel.text = "\(content.creationDate)"
        cell.titleLabel.textColor = UIColor.black
        if let imageURL = content.imageUrl {
            cell.imageView.kf.setImage(with: URL(string: imageURL))
        } else {
            FacebookAPI.downloadFacebookPost(post: content) { (url, error) in
                if let imageURL = content.imageUrl {
                    cell.imageView.kf.setImage(with: URL(string: imageURL))
                }
            }
            
        }
        cell.imageView.layer.cornerRadius = 10
        cell.imageView.layer.masksToBounds = true
    }
    
    override func didSelectContent(_ content: FacebookPost, at indexPath: IndexPath, in collectionView: UICollectionView) {
    }
}
