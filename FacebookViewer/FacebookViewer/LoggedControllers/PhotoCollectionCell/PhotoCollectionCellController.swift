//
//  PhotoCollectionCellController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 26/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit
import Tapptitude

class PhotoCollectionCellController: CollectionCellController<FacebookPicture, PhotoCollectionCell> {
    
    var onSelection: ((Int) -> ())?
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 100.0))
        minimumInteritemSpacing = 3
        minimumLineSpacing = 3
        sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
    }
    
    //    custom cell size
    override func cellSize(for content: FacebookPicture, in collectionView: UICollectionView) -> CGSize {
        let itemSize = UIScreen.main.bounds.width / 3 - 3
        return CGSize(width: itemSize, height: itemSize)
    }
    
    override func configureCell(_ cell: PhotoCollectionCell, for content: FacebookPicture, at indexPath: IndexPath) {
        if let imageURL = content.pictureUrl {
            cell.imageView.kf.setImage(with: URL(string: imageURL))
        } else {
            FacebookAPI.downloadFacebookPhotoWith(id: content.id, bestQuality: false) { (url, error) in
                if let imageURL = url {
                    cell.imageView.kf.setImage(with: URL(string: imageURL))
                }
            }
        }
    }
    
    override func didSelectContent(_ content: FacebookPicture, at indexPath: IndexPath, in collectionView: UICollectionView) {
        onSelection?(indexPath.row)
    }
}
