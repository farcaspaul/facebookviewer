//
//  PhotoCollectionCell.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 26/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit

class PhotoCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
