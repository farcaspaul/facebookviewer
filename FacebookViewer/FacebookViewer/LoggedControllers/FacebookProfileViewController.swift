//
//  FacebookProfileViewController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 06/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import FacebookLogin
import FBSDKLoginKit
import Tapptitude

class FacebookProfileViewController: UIViewController, UIViewControllerPreviewingDelegate {
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    
    var feedController = CollectionFeedController()
    var dataSource: DataSource<Any>? {
        return self.feedController.dataSource as? DataSource<Any>
    }
    
    var isMessagePresenting = { (viewController: FacebookProfileViewController) -> (Bool) in
        if let visible = viewController.presentedViewController {
            if visible.isKind(of: UIAlertController.self) {
                return true
            }
        }
        return false
    }
    
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        feedController.creator = self
        feedController.collectionView = self.collectionView
        let albumCellController = AlbumCellController()
        albumCellController.onSelection = {
            let controller = FacebookAlbumsViewController.instantiateFromLoggedStoryboard()
            
            self.navigationController?.pushViewController(controller, animated: true)
        }
        let profileCellController = ProfileCellController()

        let parallelFeed = ParallelDataFeed()
        parallelFeed.reloadOperation.append(operation: FacebookAPI.downloadFacebookProfile(callback:))
        parallelFeed.reloadOperation.append(operation: FacebookAPI.downloadFacebookAlbumsCount(callback:))
        parallelFeed.reloadOperation.append(operation: FacebookAPI.downloadFacebookFeed(offset:callback:))
        
        parallelFeed.loadMoreOperation.append(operation: FacebookAPI.downloadFacebookFeed(offset:callback:))
        
        let datasource = DataSource<Any>()
        datasource.feed = parallelFeed
        feedController.cellController = MultiCollectionCellController([FeedCellController(), profileCellController, albumCellController])
        feedController.dataSource = datasource
        
        feedController.reloadSpinnerView = activityIndicator
        feedController.addPullToRefresh()
        feedController.cellController.parentViewController = self
        
        if( traitCollection.forceTouchCapability == .available){
            registerForPreviewing(with: self, sourceView: self.feedController.collectionView)
        }
        
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = feedController.collectionView?.indexPathForItem(at: location) else { return nil }

        guard let cell = feedController.collectionView?.cellForItem(at: indexPath) else { return nil }
        if (!cell.isKind(of: ProfileCell.self)) {return nil}
        let controller = PhotosInspectorViewController.instantiateFromLoggedStoryboard()
        controller.currentItem = 0
        let tempPicture = FacebookPicture()
        tempPicture.pictureUrl = (cell as! ProfileCell).profile.profilePictureUrl
        controller.pictures = [tempPicture]

        controller.preferredContentSize = CGSize(width: 0.0, height: 300)
        previewingContext.sourceRect = cell.frame
        return controller
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        show(viewControllerToCommit, sender: self)
    }

    private func logout(action: UIAlertAction) {
        LoginManager.init().logOut()
        let controller = LoginViewController.instantiateFromNotLoggedStoryboard()
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.changeRootController(with: controller)
    }
    
    @IBAction func logoutAction(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Warning", message: "About to log out", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Log out", style: UIAlertAction.Style.default, handler: logout))
        self.present(alert, animated: true, completion: nil)
    }
}
