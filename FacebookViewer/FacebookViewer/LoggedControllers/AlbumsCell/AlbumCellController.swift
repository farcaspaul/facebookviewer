//
//  AlbumCellController.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 21/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit
import Tapptitude

class AlbumCellController: CollectionCellController<Int, AlbumCell> {
        
    var onSelection: (() -> ())?
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 50.0))
        minimumInteritemSpacing = 10
        minimumLineSpacing = 20
        sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
    
    //    custom cell size
    //    override func cellSize(for content: FacebookPost, in collectionView: UICollectionView) -> CGSize {
    //        return <#code#>
    //    }
    
    override func configureCell(_ cell: AlbumCell, for content: Int, at indexPath: IndexPath) {
        cell.titleLabel.text = "Albums: \(content)"
        cell.imageView.layer.masksToBounds = true
        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = true
    }
    
    override func didSelectContent(_ content: Int, at indexPath: IndexPath, in collectionView: UICollectionView) {
        onSelection?()
    }
}

