//
//  AlbumCell.swift
//  FacebookViewer
//
//  Created by Paul Farcas on 21/11/2018.
//  Copyright © 2018 Paul Farcas. All rights reserved.
//

import UIKit

class AlbumCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
